# Apache nifi role
## Use
```bash
cd example

# Edit inventory
vim inventory/hosts

# Edit configs
vim roles/nifi/defaults/main.yml

# Install
ansible-playbook 10-nifi_install.yml

# Configure and restart
ansible-playbook 20-nifi_configure.yml
```
